import java.io.*;
import java.net.*;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


class RaspberryInfo {
    private static final String POST_URL = "http://lumos-ip.herokuapp.com/ip";

    public static void main(String args[]) throws Exception {

        TimeUnit.SECONDS.sleep(30);
        String myIp = "";
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface iface = interfaces.nextElement();
                // filters out 127.0.0.1 and inactive interfaces
                if (iface.isLoopback() || !iface.isUp())
                    continue;

                Enumeration<InetAddress> addresses = iface.getInetAddresses();
                while(addresses.hasMoreElements()) {
                    InetAddress addr = addresses.nextElement();
                    myIp = addr.getHostAddress();
                }
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
        HashMap<String, String> info = new HashMap<String, String>();

        info.put("ip_address", myIp);
        info.put("wifi_name", "");

        sendPOST(info);

    }

    private static void sendPOST(HashMap<String, String> map) throws IOException {
        URL url = new URL(POST_URL);
        HashMap<String,String> params = map;

        StringBuilder postData = new StringBuilder();
        for (HashMap.Entry<String,String> param : params.entrySet()) {
            if (postData.length() != 0) postData.append('&');
            postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
            postData.append('=');
            postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
        }
        byte[] postDataBytes = postData.toString().getBytes();

        HttpURLConnection conn = (HttpURLConnection)url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
        conn.setDoOutput(true);
        conn.getOutputStream().write(postDataBytes);

        Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

        for (int c; (c = in.read()) >= 0;)
            System.out.print((char)c);
    }
}